//
//  PostTableViewCell.swift
//  Mobileappchallenge
//
//  Created by hungnguyen on 11/15/17.
//  Copyright © 2017 hungnguyen. All rights reserved.
//

import UIKit

class PostTableViewCell: UITableViewCell {
	
	@IBOutlet weak var avatar: UIImageView! {
		didSet {
			avatar.layer.masksToBounds = true
			avatar.layer.cornerRadius = avatar.frame.height / 2
		}
	}
	
	@IBOutlet weak var timePost: UILabel!
	@IBOutlet weak var name: UILabel!
	@IBOutlet weak var likes: UILabel!
	@IBOutlet weak var comments: UILabel!
	@IBOutlet weak var message: UILabel!
	@IBOutlet weak var postImage: UIImageView!
	
	private let webservice = WebService.shared
	private let cachedImage = Cache.shared
	private var genaration = 0
	
	
	override func prepareForReuse() {
		super.prepareForReuse()
		timePost.text = ""
		postImage.image = nil
		avatar.image = nil
		genaration += 1
		likes.text = "  likes"
		comments.text = "  comments"
	}
	
	func config(with postEntity: PostEntity) {
		let currentGenaration = genaration
		
		// MARK: - Image post
		cachedImage.loadImage(with: postEntity.imageURLString!) { [weak self] (image) in
			if currentGenaration == self?.genaration {
				DispatchQueue.main.async {
					self?.postImage.image = image
				}
			}
		}
		
		message.text = postEntity.message
		
		
		// MARK: - Fetch like, comment
		webservice.fetch(with: "/\(postEntity.id)/likeCount") { [weak self] (reponse: CountResponse?) in
			guard let data = reponse else {
				return
			}
			if currentGenaration == self?.genaration {
				DispatchQueue.main.async {
					self?.likes.text = "\(data.count) likes"
				}
			}
		}
		
		webservice.fetch(with: "/\(postEntity.id)/commentCount") { [weak self] (reponse: CountResponse?) in
			guard let data = reponse else {
				return
			}
			if currentGenaration == self?.genaration {
				DispatchQueue.main.async {
					self?.comments.text = "\(data.count) comments"
				}
			}
		}
		
		
		// MARK: - POST TIME
		if let postTime = postEntity.postAt {
			if  let date = dateFormat.date(from: postTime)  {
				let _date = date as NSDate
				timePost.text = dateFormat.timeSince(from: _date)
			}
		}
		
		
		// MARK:  Load user
		guard let user = postEntity.user else  {
			return
		}

		name.text = user.fullName

		guard let userImage = user.image else {
			return
		}
		cachedImage.loadImage(with: userImage) { [weak self] (image) in
			if currentGenaration == self?.genaration {
				DispatchQueue.main.async {
					self?.avatar.image = image
				}
			}
		}
	}
}


