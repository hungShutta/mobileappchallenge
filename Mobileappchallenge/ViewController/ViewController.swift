//
//  ViewController.swift
//  Mobileappchallenge
//
//  Created by hungnguyen on 11/15/17.
//  Copyright © 2017 hungnguyen. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	@IBOutlet weak var tableView: UITableView!
	
	var posts = [PostEntity]() {
		didSet {
			DispatchQueue.main.async {
				self.tableView.reloadData()
			}
		}
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		tableView.dataSource = self
		
		fetchData() { }
	}
	
	
	func fetchData(completionHandler: @escaping () -> Void ) {
		print("Fetch data")
		let coreDataStack = CoreDataStack.shared
		// First get data from core data
		let posts = coreDataStack.getPost()
		
		self.posts = posts
		
		WebService.shared.fetchSupportOffLine(with: "") { [weak self] (posts: [PostEntity]?) in
			self?.posts = posts!
		}
	}
}

// MARK: - UITableViewDataSource
extension ViewController : UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return posts.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "UITableViewCellI", for: indexPath) as! PostTableViewCell
		cell.config(with: posts[indexPath.row])
		
		return cell
	}
}

