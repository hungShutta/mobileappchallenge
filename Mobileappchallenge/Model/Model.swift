//
//  Model.swift
//  Mobileappchallenge
//
//  Created by hungnguyen on 11/15/17.
//  Copyright © 2017 hungnguyen. All rights reserved.
//

import Foundation

struct CountResponse {
	let code: Int
	let status: String
	let count: Int
}

extension CountResponse: JSONDecodable {
	init(_ decoder: JSONDecoder) throws {
		self.code 		= try decoder.value(forKey: "code")
		self.status 	= try decoder.value(forKey: "status")
		self.count 	= try decoder.value(forKey: "data")
	}
}

struct Response {
	let code: Int
	let status: String
	fileprivate let dataJSON: [JSONObject]
	
	var data: [Post]? {
		return try? dataJSON.map(decode)
	}
}

extension Response: JSONDecodable {
	init(_ decoder: JSONDecoder) throws {
		self.code 		= try decoder.value(forKey: "code")
		self.status 	= try decoder.value(forKey: "status")
		self.dataJSON 	= try decoder.value(forKey: "data")
	}
}


struct Post {
	let id: Int64
	fileprivate let userJSon: JSONObject
	let message: String
	let postedAt: String
	let imageURLString: String
	
	var user: User? {
		return try? decode(userJSon)
	}
}

extension Post: JSONDecodable {
	init(_ decoder: JSONDecoder) throws {
		self.id 			= try decoder.value(forKey: "_id")
		self.userJSon 		= try decoder.value(forKey: "user")
		self.message 		= try decoder.value(forKey: "message")
		self.postedAt 		= try decoder.value(forKey: "postedAt")
		self.imageURLString = try decoder.value(forKey: "imageURL")
	}
}

struct User {
	let id: Int64
	let fullName: String
	let mobileNo: String
	let dob: String
	let imageURLString: String
}

extension User: JSONDecodable {
	init(_ decoder: JSONDecoder) throws {
		self.id 			= try decoder.value(forKey: "_id")
		self.fullName 		= try decoder.value(forKey: "fullName")
		self.mobileNo 		= try decoder.value(forKey: "mobileNo")
		self.dob 			= try decoder.value(forKey: "dob")
		self.imageURLString = try decoder.value(forKey: "imageURL")
	}
}

