//
//  CoreDataLayer.swift
//  AstroAssigment
//
//  Created by hungnguyen on 11/4/17.
//  Copyright © 2017 hungnguyen. All rights reserved.
//

import Foundation
import CoreData

final class CoreDataStack {
	static let shared = CoreDataStack()
	
	lazy var appplicationDocumentDirectory: URL = {
		let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
		return urls.last!
	}()
	
	lazy var persistenContainer: NSPersistentContainer = {
		let _persistenContainer = NSPersistentContainer(name: "USEREXP")
		_persistenContainer.loadPersistentStores  { (description, error) in
			if let error = error {
				fatalError("Failed to load Core Data stack: \(error)")
			}
		}
		return _persistenContainer
	}()
	
	
	private init() {
	}
}

extension CoreDataStack {
	func trySave() {
		do {
			try self.persistenContainer.viewContext.save()
		} catch {
			print("\(error.localizedDescription)")
		}
	}
	
	func removeData(with entityName: String, completion handler: (Error?) -> Void) {
		let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
		let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
		
		do {
			try self.persistenContainer.viewContext.execute(deleteRequest)
			try self.persistenContainer.viewContext.save()
			handler(nil)
		} catch {
			handler(error)
			print("\(error.localizedDescription)")
		}
	}
	
	func saveInBackground(posts: [Post], completion handler: @escaping (Error?) -> Void) {
		
		removeData(with: "PostEntity") { (error) in
			if let error = error {
				print("Error importing messages: \(error.localizedDescription)")
			}
		}
		
		persistenContainer.performBackgroundTask { (context) in
			do {
				for post in posts {
					let despost = NSEntityDescription.entity(forEntityName: "PostEntity", in: context)
					let postEntity = PostEntity(entity: despost!, insertInto: context)
					postEntity.id = post.id
					postEntity.imageURLString = post.imageURLString
					postEntity.postAt = post.postedAt
					postEntity.message = post.message
					
					
					let userEntity: UserEntity
					
					if let _user = postEntity.user {
						userEntity = _user
					} else {
						let desUserEntity = NSEntityDescription.entity(forEntityName: "UserEntity", in: context)
						userEntity = UserEntity(entity: desUserEntity!, insertInto: context)
						postEntity.user = userEntity
					}
					
					guard let user = post.user else {
						continue
					}
					userEntity.id = user.id
					userEntity.fullName = user.fullName
					userEntity.image = user.imageURLString
				}
				try context.save()
				handler(nil)
			} catch {
				handler(error)
				print("Error importing messages: \(error.localizedDescription)")
			}
		}
	}
	
	func getPost() -> [PostEntity]  {
		let fetchRequest: NSFetchRequest<PostEntity> = PostEntity.fetchRequest()
		let sort = NSSortDescriptor(key: "id", ascending: false)
		fetchRequest.sortDescriptors = [sort]
		let entities = try! self.persistenContainer.viewContext.fetch(fetchRequest)
		
		print(entities.map { $0.id})
		return entities
	}
	func getEntity<T: NSManagedObject>() -> [T]? {
		let fetchRequest = T.fetchRequest()
		let entity = try! self.persistenContainer.viewContext.fetch(fetchRequest) as? [T]
		
		return entity
	}
}


