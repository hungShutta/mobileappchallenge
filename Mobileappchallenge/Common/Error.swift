//
//  Error.swift
//  Mobileappchallenge
//
//  Created by hungnguyen on 11/16/17.
//  Copyright © 2017 hungnguyen. All rights reserved.
//

import Foundation

enum NetworkError: Error {
	case urlBroken
	case missingData
	case unknow
}

enum JSONParsingError: Error {
	case missingKey(key: String)
	case typeMismatch(key: String)
	case badJSONToObject
	case badJSONToArray
	case unknow
}



