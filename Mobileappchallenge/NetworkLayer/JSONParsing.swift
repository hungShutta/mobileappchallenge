//
//  JSONParsing.swift
//  Mobileappchallenge
//
//  Created by hungnguyen on 11/15/17.
//  Copyright © 2017 hungnguyen. All rights reserved.
//

import Foundation

protocol JSONDecodable {
  	init(_ decoder: JSONDecoder) throws
}

typealias JSONObject = [String: Any]

final class JSONDecoder {
  let jsonObject: JSONObject
  init(_ jsonObject: JSONObject) {
    self.jsonObject = jsonObject
  }
  
  func value<T>(forKey key: String) throws -> T {
    guard let value = jsonObject[key] else {
      throw JSONParsingError.missingKey(key: key)
    }
    guard let finalValue = value as? T else {
      throw JSONParsingError.typeMismatch(key: key)
    }
    return finalValue
  }
}

func parse<T>(_ data: Data) throws -> [T] where T: JSONDecodable {
  	let jsonObjects: [JSONObject] = try deserialize(data)
  	return try jsonObjects.map(decode)
}

func parse<T>(_ data: Data) throws -> T where T: JSONDecodable {
	let jsonObject: JSONObject = try deserialize(data)
	return try decode(jsonObject)
}

func deserialize(_ data: Data) throws -> [JSONObject] {
  	let json = try JSONSerialization.jsonObject(with: data, options: [])
  	guard let objects = json as? [JSONObject] else {
		throw JSONParsingError.badJSONToArray
	}
  return objects
}

func deserialize(_ data: Data) throws -> JSONObject {
	let json = try JSONSerialization.jsonObject(with: data, options: [])
	guard let object = json as? JSONObject else {
		throw JSONParsingError.badJSONToObject
	}
	
	return object
}

func decode<T>(_ jsonObject: JSONObject) throws -> T where T: JSONDecodable {
  return try T.init(JSONDecoder(jsonObject))
}
















