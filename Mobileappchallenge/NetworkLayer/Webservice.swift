//
//  Webservice.swift
//  Mobileappchallenge
//
//  Created by hungnguyen on 11/15/17.
//  Copyright © 2017 hungnguyen. All rights reserved.
//

import Foundation
import CoreData
final class WebService {
	static let shared = WebService()
	
	private init() { }
	
	fileprivate let baseURL = "http://thedemoapp.herokuapp.com/post"
	
	fileprivate let coreDataStack = CoreDataStack.shared
	
	fileprivate lazy var session: URLSession = {
		let config = URLSessionConfiguration.default
		return URLSession(configuration: config)
	}()
}

extension WebService {
	func fetch<T>(with path: String,
					   completion handler: @escaping (T?) -> Void )where T: JSONDecodable {
		let urlString = baseURL.appending(path)
		guard let url = URL(string: urlString) else {
			handler(nil)
			return
		}
		
		let task = session.dataTask(with: url) { (data, _, error) in
			do {
				guard error == nil, let data = data else {
					throw NetworkError.missingData
				}
				
				let reponses: T = try parse(data)
				
				handler(reponses)
			} catch {
				handler(nil)
				print("Error: \(error.localizedDescription)")
			}
		}
		task.resume()
	}
	
	func fetchSupportOffLine<T: NSManagedObject>(with path: String,
												 completion handler: @escaping ([T]?) -> Void ) {
			
		// First fet from data base
		let entities: [T]? = coreDataStack.getEntity()
		handler(entities)
		
		//Then make api call
		fetchNetwork(with: path) { [weak self] (response: Response?) in
			guard let post = response?.data else {
				return
			}
			self?.coreDataStack.saveInBackground(posts: post) { (error) in
				guard error == nil else {
					print("Error when save \(String(describing: error?.localizedDescription))")
					return
				}
				
				// after call fire handler again
				let entities: [T]? = self?.coreDataStack.getEntity()
				handler(entities)
			}
		}
	}
	
	private func fetchNetwork<T>(with path: String,
						 completion handler: @escaping (T?) -> Void ) where T: JSONDecodable {
		
		let urlString = baseURL.appending(path)
		guard let url = URL(string: urlString) else {
			handler(nil)
			return
		}
		
		let task = session.dataTask(with: url) { (data, _, error) in
			do {
				guard error == nil, let data = data else {
					throw NetworkError.missingData
				}
				let reponse: T = try parse(data)
				handler(reponse)
			} catch {
				handler(nil)
				print("Error: \(error.localizedDescription)")
			}
		}
		task.resume()
	}
}
